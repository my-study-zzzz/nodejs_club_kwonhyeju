const mongoose =require('mongoose');
const moment = require('moment');

const ClubSchema = new mongoose.Schema({
    name:{
        type : String,
        required : true,
        minlength: [3, '클럽이름은 3자 이상입력해주세요']
    },
    intro:{
        type : String,
        required : true,
        minlength: [10, '클럽인트로는 10자 이상입력해주세요']
    },
    image:{
        type : String,
    },
    membershipList : [],
    foundationDay:{
        type : Date,
        default : Date.now
    },
    memberCount:{
        type:Number,
        default:1
    }
});

const Club = mongoose.model('Club',ClubSchema);

module.exports = Club;


