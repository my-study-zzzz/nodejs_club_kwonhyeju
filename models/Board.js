const mongoose =require('mongoose');
const moment = require('moment');

const BoardSchema = new mongoose.Schema({
    clubId:{
        type : String,
        required : true,
    },
    name:{
        type : String,
        required : true,
    },
    createDate:{
        type : Date,
        default : Date.now
    },
    postingCount:{
        type:Number,
        default:0
    }
});

const Board = mongoose.model('Board',BoardSchema);

module.exports = Board;


