const mongoose =require('mongoose');

const UserSchema = new mongoose.Schema({
    name:{
        type : String,
        required : true
    },
    email:{
        type : String,
        required : true
    },
    password:{
        type : String,
        required : true
    },
    phonenumber:{
        type: String,
        required : true
    },
    nickname:{
        type: String,
        required : true
    },
    birthday:{
        type : Date,
        required : true
    },
    registerdate:{
        type : Date,
        default : Date.now
    },
    notiCount:{
        type:Number,
        default:0
    }
});

const User = mongoose.model('User',UserSchema);

module.exports = User;


