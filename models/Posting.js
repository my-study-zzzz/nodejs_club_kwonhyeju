const mongoose =require('mongoose');

const PostingSchema = new mongoose.Schema({
    boardId:{
        type : mongoose.Schema.Types.ObjectId, 
        ref: 'Board',
        required : true
    },
    clubId:{
        type : mongoose.Schema.Types.ObjectId, 
        ref: 'Club',
        required : true
    },
    memId:{
        type : mongoose.Schema.Types.ObjectId, 
        ref: 'User',
        required : true
    },
    title:{
        type : String,
        required : true
    },
    content:{
        type : String,
        required : true
    },
    writtendate:{
        type : Date,
        default : Date.now
    },
    sequence:{
        type:Number,
        default:1
    },
    click:{
        type:Number,
        default:0
    },
    like:{
        type:Number,
        default:0
    },
    commentCount:{
        type:Number,
        default:0
    }
});

const Posting = mongoose.model('Posting',PostingSchema);

module.exports = Posting;


