const mongoose =require('mongoose');

const CommentSchema = new mongoose.Schema({
    postingId:{
        type : mongoose.Schema.Types.ObjectId, 
        ref: 'Posting',
        required : true
    },
    memId:{
        type : mongoose.Schema.Types.ObjectId, 
        ref: 'User',
        required : true
    },
    content:{
        type : String,
        required : true
    },
    writtendate:{
        type : Date,
        default : Date.now
    },
    like:{
        type:Number,
        default:0
    }
});

const Comment = mongoose.model('Comment',CommentSchema);

module.exports = Comment;


