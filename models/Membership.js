const mongoose =require('mongoose');

const MembershipSchema = new mongoose.Schema({
    clubId:{
        type : mongoose.Schema.Types.ObjectId, 
        ref: 'Club',
        required : true
    },
    memId:{
        type : mongoose.Schema.Types.ObjectId, 
        ref: 'User',
        required : true
    },
    email:{
        type : String,
        required : true
    },
    role:{
        type : String,
        required : true
    },
    joindate:{
        type : Date,
        default : Date.now
    }

});

const Membership = mongoose.model('Membership',MembershipSchema);

module.exports = Membership;


