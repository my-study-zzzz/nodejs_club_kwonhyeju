const mongoose =require('mongoose');

const NotiSchema = new mongoose.Schema({
    memId:{
        type : mongoose.Schema.Types.ObjectId, 
        ref: 'User',
        required : true
    },
    title:{
        type : String,
        required : true
    },
    content:{
        type : String,
        required : true
    },
    writtendate:{
        type : Date,
        default : Date.now
    },
    new:{
        type:Boolean,
        default:true
    }
});

const Noti = mongoose.model('Noti',NotiSchema);

module.exports = Noti;


