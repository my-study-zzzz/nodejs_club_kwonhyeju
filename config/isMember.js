//Membership model
const Membership = require('../models/Membership');

module.exports = {
    isMember: function(req, res, next){
        const clubId = req.params.clubId == null? req.query.clubId : req.params.clubId ;

        Membership.findOne({clubId:clubId, memId:req.user._id})
        .then(membership=>{
            if(membership != null){//클럽회원이 맞다면,
                return next();
            }else{//해당멤버가 클럽회원이 아니라면,
                res.redirect('/clubWindow/noMember/'+clubId);
            }
        })
       
    }


}