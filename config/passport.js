const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

// Load user model
const User = require('../models/User');

module.exports = function(passport){
    passport.use(
        new LocalStrategy({ usernameField: 'email' }, (email,password,done)=>{
            //Match user
            User.findOne({email:email})
            .then(user => {

                //there is no user that has this email
                if(!user){//undefinded == false / !undefinded == true
                    return done(null, false, 
                        {
                            message: '해당 이메일로 등록된 회원이 없습니다.',
                            
                        });
                }

                //Match password. 알아서 복호화해주나봄
                bcrypt.compare(password, user.password, (err, isMatch)=>{
                    if(err) throw err;

                    if(isMatch){
                        return done(null, user);
                    } else{
                        return done(null, false, { message:'비밀번호가 일치하지 않습니다.'});
                    }
                });
            })
            .catch(err => console.log(err));
        })
    );

    //이거 왜 해? 직렬화 역직렬화 : 이걸해야 session 유지가능
    passport.serializeUser((user, done)=> {// req.session.passport.user에 세션에 저장하는 과정입니다.
        done(null, user.id);
      });
      
      passport.deserializeUser((id, done) => { // 세션에 저장되어있는 값을 DB와 비교하는 과정입니다.
        User.findById(id, function(err, user) {
          done(err, user);
        });
      });
}