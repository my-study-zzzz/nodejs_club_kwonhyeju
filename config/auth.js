module.exports = {
    ensureAuthenticated: function(req, res, next){
        if(req.isAuthenticated()){
            return next();
        }
        req.flash('error_msg', '서비스를 이용하시려면 로그인 부탁드립니다.');
        res.redirect('/users/login');
    }

}