const express = require('express');
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth');
const bcrypt = require('bcryptjs');
const async = require('async');

//user model
const User = require('../models/User');
//Membership model
const Membership = require('../models/Membership');
//Club model
const Club = require('../models/Club');
//Noti model
const Noti = require('../models/Noti');

//mypage 접속시 - 바로 updateUser
router.get('/', ensureAuthenticated, (req, res) => {
    res.render('./mypage/updateUser',
        {
            req: req,
            user: req.user,
            birthday: req.user.birthday.yyyymmdd(),
            email: req.user.email,
            name: req.user.name,
            nickname: req.user.nickname,
            phonenumber: req.user.phonenumber
        });
});

//mypage - updatePassword
router.get('/updatePassword', ensureAuthenticated, (req, res) => {
    res.render('./mypage/updatePassword', { req: req, user: req.user });
});

//mypage - withdrawUser
router.get('/withdrawUser', ensureAuthenticated, (req, res) => {
    res.render('./mypage/withdrawUser', { req: req, user: req.user });
});

//POST - 회원정보 수정하기
router.post('/updateUser', async (req, res) => {
    const { email, name, nickname, phonenumber, birthday } = req.body;
    let errors = [];

    //check required fields
    if (!name || !nickname || !phonenumber || !birthday) {
        errors.push({ msg: 'Please fill in all fields' });
    }
    //check nickname duplication
    await User.findOne({ email: { $ne: email }, nickname: nickname })
        .then(user => {
            if (user)
                errors.push({ msg: '해당 닉네임이 이미 존재합니다.' });
        });


    if (errors.length > 0) {
        res.render('./mypage/updateUser', {
            errors,
            req: req,
            user: req.user,
            name,
            email,
            nickname,
            phonenumber,
            birthday
        });
    } else {
        // validation passed
        User.updateOne({ email: email },
            {
                $set: {
                    name: name,
                    nickname: nickname,
                    birthday: birthday,
                    phonenumber: phonenumber
                }
            })
            .then(user => {
                req.flash("success_msg", '수정완료!')
                res.redirect('/mypage');
            });
    }
});

//POST - 비밀번호 변경하기
router.post('/updatePassword', (req, res) => {
    const { passwordCur, password, password2 } = req.body;
    const email = req.user.email;
    let errors = [];

    //check nickname duplication
    let userPwd = '';
    User.findOne({ email: email })
        .then(user => {
            userPwd = user.password;
        })
        .then(user => {
            //Match password. 알아서 복호화해주나봄
            return new Promise(function(resolve){
                bcrypt.compare(passwordCur, userPwd, (err, isMatch) => {
                    if (err) throw err;
                    if (!isMatch) {
                        errors.push({ msg: '비밀번호를 정확하게 입력해주세요! ' });
                    }
                    resolve();
                });
            });
        })
        .then(data => {
            console.log('3번재 data:', data);
            //check pass length
            if (password.length < 4) {
                errors.push({ msg: '비밀번호는 최소 4자 이상 입력바랍니다.' });
            }
            //check passwords match
            if (password !== password2) {
                errors.push({ msg: '새 비밀번호와 비밀번호 확인이 일치하지 않습니다.' });
            }
            if (errors.length > 0) {
                res.render('./mypage/updatePassword', {
                    errors,
                    req: req,
                    user: req.user,
                    passwordCur,
                    password,
                    password2
                });
            } else {
                // validation passed

                // Hash Password (비밀번호 암호화)
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(password, salt, (err, hash) => {
                        if (err) throw err;

                        //set password to hashed
                        //save user
                        User.updateOne({ email: email }, { $set: { password: hash } })
                            .then(user => {
                                req.flash("success_msg", '수정완료!')
                                res.redirect('/mypage/updatePassword');
                            });
                    })
                });


            }
        });


});

//탈퇴하기 : 
router.get('/withdrawUserLast/:passwordCur', (req, res) => {
    const passwordCur = req.params.passwordCur;
    const email = req.user.email;
    let errors = [];

    User.findOne({ email: email }).select('password')
    .then(pwd=>{
        return new Promise(function(resolve){
            bcrypt.compare(passwordCur, pwd.password, (err, isMatch) => {
                if (err) throw err;
                if (!isMatch) {
                    errors.push({ msg: '비밀번호를 정확하게 입력해주세요! ' });
                }
                resolve();
            });
        });
    })
    .then(bcrypt=>{
        if (errors.length > 0) {
            res.render('./mypage/withdrawUser', {
                errors,
                req: req,
                user: req.user,
                passwordCur,
            });
        } else {
            
       
                const userId = req.user._id;
                let clubArr = new Array();
                Membership.find({memId: userId})//해당 유저의 멤버십 반환
                .then(memberships=>{
                    for(ms of memberships){
                        clubArr.push(ms.clubId);
                    }

                    // Club.updateMany({_id:{$in:clubArr}},{$inc:{memberCount:-1}})
                    // .then(result=>{
                        //     Membership.deleteMany({memId: user._id})
                        //     .then(result=>{
                            //         req.flash("success_msg", '탈퇴처리가 완료되었습니다.');
                            //         res.redirect('/users/login');
                            //     })
                            // })
                            
                            
                    //async.퍼렐 : 1. 클럽.멤버카운트-1  2. 멤버십삭제 3. 노티삭제 4. 유저삭제
                    ///////////////////
                    async.parallel({
                        clubUpdate: function (callback) { Club.updateMany({_id:{$in:clubArr}},{$inc:{memberCount:-1}}).exec((err, data) => callback(null, data)) },
                        membershipDelete: function (callback) { Membership.deleteMany({memId: userId}).exec((err, data) => callback(null, data)) },
                        notiDelete: function (callback) {Noti.deleteMany({memId: userId}).exec((err,data)=> callback(null,data))},
                        userDelete: function (callback) {User.deleteOne({_id:userId}).exec((err,data)=> callback(null,data))},
                        
                      }, function (err, result) {
                        req.flash("success_msg", '탈퇴처리가 완료되었습니다.');
                        res.redirect('/users/login');
                      });

                    /////////////////////
                })
        }
    })
});


// yyyymmdd 형태로 포멧팅하여 날짜 반환
Date.prototype.yyyymmdd = function () {
    let yyyy = this.getFullYear().toString();
    let mm = (this.getMonth() + 1).toString();
    let dd = this.getDate().toString();

    return yyyy + '-' + (mm[1] ? mm : '0' + mm[0]) + '-' + (dd[1] ? dd : '0' + dd[0]);
}

module.exports = router;
