var express = require('express');
var router = express.Router();
const clubLogic = require('../public/javascripts/logic/clubLogic');
const membershipController = require('../public/javascripts/controller/membershipController');
const { ensureAuthenticated } = require('../config/auth');
const async = require('async');

//club model
const Club = require('../models/Club');
//Membership model
const Membership = require('../models/Membership');
//user model
const User = require('../models/User');
//user model
const Noti = require('../models/Noti');



//운영클럽 관리 페이지로 이동
router.get('/:id', ensureAuthenticated, (req, res) => {
  const id = req.params.id;
  //클럽에 가입한 멤버들
  let clubName = '';
  Club.findById({ _id: id }, { name: 1, _id: 0 })
    .then(result => clubName = result.name)
    .then(result => {
      Membership.find({ clubId: req.params.id }).populate('memId')
        .then(members => {
          // let memJson = JSON.stringify(members)
          res.render('./club/memberManage',
            { req: req, user: req.user, clubActive: 'active', id: id, members: members, name: clubName });
        });
    });
});

//운영클럽 - 강제탈퇴 : 멤버십 한개 삭제
router.get('/remove/:membershipId', (req, res) => {
  //멤버십삭제 =>클럽에서 멤버십 -1  => 알림주기
  Membership.findOneAndDelete({ _id: req.params.membershipId }, function (err, membership) {

    async.waterfall([
      function (next) {
        Club.findOneAndUpdate({ _id: membership.clubId }, { $inc: { memberCount: -1 } }).exec((err, club) => {
          if (club == null) res.json({ message: 'FAIL' });
          else next(err, club);
        });
      },
      function (club, next) {
        new Noti({ memId: membership.memId, title: '탈퇴알림', content: `운영자에 의해 ${club.name}에서 탈퇴처리되셨습니다.` })
          .save((err, noti) => {
            next(err, noti);
          });
      }
    ], function (err, result) {
      if (err) res.json({ message: 'FAIL' });
      res.json({ message: 'SUCCESS' })
    });
  });


});

//운영클럽 - 운영자 권한 위임
router.get('/changePresident/:memberId',(req,res)=>{
  //1. 현 운영자의 권한 => member

  //2. 새 운영자의 권한 => president

  //3. 멤버들에게 운영자 바꼈다고 알림
  const clubId = req.query.clubId;
  
  async.parallel({
    orgPresident: function (callback) { Membership.findOneAndUpdate({memId:req.user._id, clubId:clubId},{$set:{role:'Member'}}).populate('memId').exec((err, data) => callback(null, data)) },
    newPresident: function (callback) { Membership.findOneAndUpdate({_id:req.params.memberId, clubId:clubId},{$set:{role:'President'}}).populate('memId').exec((err, data) => callback(null, data)) },
    club: function (callback) { Club.findOne({ _id: clubId }).exec((err, data) => callback(null, data)) },
  }, function (err, result) {
    const { orgPresident, newPresident,club } = result;
    const orgNick = orgPresident.memId.nickname;
    const newNick = newPresident.memId.nickname;
    console.log('원래운영자:',orgPresident.memId.nickname);
    console.log('새운영자:',newPresident.memId.nickname);
    // console.log('원래운영자:',orgPresident.role);

    Membership.find({ clubId: clubId })
    .then(members => {
      for (member of members) {
        new Noti({ memId: member.memId, title: '클럽변경 알림', content: `${club.name}의 운영자가 변경되었습니다.\r\n기존운영자 : ${orgNick}\r\n새 운영자 : ${newNick}` })
          .save(result => { console.log('저장',result);  });
      }

      res.json({message:'SUCCESS'});
    });

  });

});




module.exports = router;
