var express = require('express');
var router = express.Router();
const { ensureAuthenticated } = require('../config/auth');
const async = require('async');

//club model
const Club = require('../models/Club');
//Membership model
const Membership = require('../models/Membership');
//user model
const User = require('../models/User');
//Board model
const Board = require('../models/Board');
//Posting model
const Posting = require('../models/Posting');
//Comment model
const Comment = require('../models/Comment');



//댓글 작성
router.post('/register', (req, res) => {
  const {content,postingId} = req.body;
  const newComment = new Comment({
    content: content,
    postingId: postingId,
    memId: req.user.id,
  });

  newComment.save((err, comment) => {
    Posting.updateOne({_id:postingId},{ $inc: { commentCount: 1 }})
    .then(result=>{ res.json(comment);});
  });

});

//댓글 불러오기
router.get('/findCommentsByPostingId/:postingId',(req,res)=>{
  Comments.find({postingId:req.params.postingId}).populate('memId')
  .then(comments=>{
    res.json(comments);
  })
  .catch(err=>{
    res.json({message:err});
  })
});

//댓글 수정
router.post('/upate', (req, res) => {
  const {content,commentId} = req.body;

  Comment.updateOne( {_id:commentId},{$set:{ content:content } }).
  then((comment) => {
    res.json(comment);
  })
  .catch(err=>{
    res.json({message:err});
  });
});

//댓글 삭제
router.get('/remove/:id',(req,res)=>{
  Comment.findByIdAndRemove(req.params.id)
  .then(comment=>{
    Posting.updateOne({_id:comment.postingId},{ $inc: { commentCount: -1 }})
     .then(result=>{ res.json({ message: 'SUCCESS' });});
  })
  .catch(err=>{
    res.json({message:err});
  });
});


module.exports = router;
