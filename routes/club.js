var express = require('express');
var router = express.Router();
const { ensureAuthenticated } = require('../config/auth');
const async = require('async');
const multer = require('multer'); // express에 multer모듈 적용 (for 파일업로드)
const upload = multer({
  storage: multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/images/upload/');
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname);
    }
  }),
});

//club model
const Club = require('../models/Club');
//Membership model
const Membership = require('../models/Membership');
//user model
const User = require('../models/User');
//Noti model
const Noti = require('../models/Noti');

//club home 
router.get('/', ensureAuthenticated, async (req, res) => {
  try {
    let clubArr = [];//내가 만든 클럽만(운영클럽)
    await Membership.find({ email: req.user.email, role: 'President' }, (er, memberships) => {

    }).then(memberships => {

      for (ms of memberships) {
        clubArr.push(ms.clubId);
      }

      Club.find({ _id: { $in: clubArr } }, (er, clubs) => {
      })
        .then(clubs => {
          res.render('./menu/club', {
            req: req,
            clubList: clubs,
            clubActive: 'active',
            user: req.user
          });
        });
    });



  } catch (err) {
    // res.json({ message: err });
    console.log(err);
  }
});

//find oneClub
router.get('/findOne/:id', ensureAuthenticated, async (req, res) => {
  try {
    const club = await Club.findById(req.params.id);
    res.json(club);
  } catch (err) {
    res.json({ message: err });
  }
});

//findByName oneClub : 운영클럽만.
router.get('/findByName/:name', ensureAuthenticated, async (req, res) => {

  try {
    let clubArr = [];//내가 만든 클럽만(운영클럽)
    await Membership.find({ email: req.user.email, role: 'President' }, (er, memberships) => {
    }).then(memberships => {
      for (ms of memberships) {
        clubArr.push(ms.clubId);
      }
      Club.find({ _id: { $in: clubArr }, name: { $regex: req.params.name } }, (er, clubs) => {
      })
        .then(clubs => {
          res.json(clubs);
        });
    });
  } catch (err) {
    res.json({ message: err });
  }

});

//findByName oneClub : 가입클럽만.
router.get('/findByNameMyClub/:name', ensureAuthenticated, async (req, res) => {

  try {
    let clubArr = [];//내가 가입한 클럽만
    await Membership.find({ email: req.user.email, role: 'Member' }, (er, memberships) => {
    }).then(memberships => {
      for (ms of memberships) {
        clubArr.push(ms.clubId);
      }
      Club.find({ _id: { $in: clubArr }, name: { $regex: req.params.name } }, (er, clubs) => {
      })
        .then(clubs => {
          res.json(clubs);
        });
    });
  } catch (err) {
    res.json({ message: err });
  }

});

//club register
router.post('/register', upload.single("file"), async (req, res) => {
  const { name, intro } = req.body;
  let errors = [];
  const file = req.file;
  const filePath = file == null ? '/images/club/beach.jpg' : '/images/upload/' + file.originalname;


  // validation passed
  await Club.findOne({ name: name })
    .then(club => {
      if (club) {
        //club exists
        errors.push({ msg: '해당 클럽명이 이미 존재합니다.' });
        req.flash('errors', errors);
        req.flash('name', name);
        req.flash('intro', intro);
        res.redirect('/club');

      } else {

        const newClub = new Club({
          name,
          intro,
          image: filePath
        });

        let memId = '';
        User.findOne({ email: req.user.email })
          .then(result => {
            memId = result._id;

            newClub.save().then(club => {
              const newMembership = new Membership({
                clubId: newClub._id,
                memId: memId,
                email: req.user.email,
                role: 'President'
              });

              newMembership.save()
                .then(membership => {
                  res.redirect('/club');
                });

            })
              .catch(err => {
                const nameErr = err.errors['name'] != null ? err.errors['name'].message : null;
                const introErr = err.errors['intro'] != null ? err.errors['intro'].message : null;
                if (nameErr)
                  errors.push({ msg: nameErr });
                if (introErr)
                  errors.push({ msg: introErr });

                req.flash('errors', errors);
                req.flash('name', name);
                req.flash('intro', intro);
                res.redirect('/club');
              });
          });




      }
    });
}
);


//club remove : 클럽 삭제 => 멤버십삭제
router.get('/remove/:id', async (req, res) => {
  try {
    await Club.findByIdAndRemove( req.params.id )
    .then(club=>{
      Membership.remove({clubId:club._id})
      .then(result=>{res.redirect('/club');});
    });
  } catch (err) {
    console.log(err);
    res.redirect('/club');
  }
});

//club remove fetch
router.get('/remove/:id', async (req, res) => {
  try {
    const removeClub = await Club.deleteOne({ _id: req.params.id });
    // req.flash('success_msg', '삭제 ㅊㅋ');
    res.json(removeClub);
  } catch (err) {
    console.log(err);
    res.json({ message: err });
  }
});


//club modify
router.post('/modify/:id', upload.single("file"),  (req, res) => {
  const { name, intro } = req.body;
  const clubId = req.params.id;
  const file = req.file;
  try {
    Club.findOne({name:name, _id: {$ne : clubId}})//내가아닌 다른클럽중 같은이름이 있으면
    .then(club=>{
      if(club != null){//이름 중복
        req.flash('error_msg', '해당이름의 클럽이 이미 존재합니다.');
        req.flash('name', name);
        req.flash('intro', intro);
        res.redirect(`/manageClub/${clubId}`);
      }else{
        if (file == null) {
           Club.findByIdAndUpdate(
            { _id: clubId },
            { $set: { name, intro } })
            .then(club => {
              ///클럽 변경후, 클럽멤버에게 알려주기.
              return new Promise(function (resolve) {
                Membership.find({ clubId: clubId, role: 'Member' })
                  .then(members => {
                    for (member of members) {
                      new Noti({ memId: member.memId, title: '클럽변경 알림', content: `${club.name}이 변경되었습니다.\r\n클럽명 : ${name}\r\n소개글 : ${intro}` })
                        .save(result => { console.log('저장',result);  });
                    }
                      resolve();
                  });
              });
            })
            .then(result => {
              console.log('렌더');
              res.redirect('/manageClub/' + req.params.id);
            });
        } else {
          const filePath = '/images/upload/' + file.originalname;
           Club.findByIdAndUpdate(
            { _id: clubId },
            { $set: { name, intro, image: filePath } })
            .then(club => {
              ///클럽 변경후, 클럽멤버에게 알려주기.
              return new Promise(function (resolve) {
                Membership.find({ clubId: clubId, role: 'Member' })
                  .then(members => {
                    console.log('들');
                    for (member of members) {
                      new Noti({ memId: member.memId, title: '클럽변경 알림', content: `${club.name}이 변경되었습니다.\r\n클럽명 : ${name}\r\n소개글 : ${intro}` })
                        .save(result => { console.log('저장',result);  });
                    }
                      resolve();
                  });
              });
            })
            .then(result => {
              console.log('렌더');
              res.redirect('/manageClub/' + req.params.id);
            });
        }
      }
    });
    

  } catch (err) {
    console.log('수정실패');
    res.redirect('/club');
  }

});




module.exports = router;
