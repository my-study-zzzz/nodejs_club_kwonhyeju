var express = require('express');
var router = express.Router();
const clubLogic = require('../public/javascripts/logic/clubLogic');
const membershipController = require('../public/javascripts/controller/membershipController');
const { ensureAuthenticated } = require('../config/auth');

//club model
const Club = require('../models/Club');
//Membership model
const Membership = require('../models/Membership');
//user model
const User = require('../models/User');



//운영클럽 관리 페이지로 이동
router.get('/:id', ensureAuthenticated,  (req, res) => {
    const id = req.params.id;
    //클럽에 가입한 멤버들
    membershipController.findMembershipByClubId(id)
    .then(result=>{
      Club.findOne({ _id: req.params.id })
      .then(club=>{
        res.render('./club/clubManage',{club:club ,req:req, user:req.user,clubActive: 'active',});
      })
    });
   
});


module.exports = router;
