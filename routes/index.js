var express = require('express');
var router = express.Router();
const { ensureAuthenticated } = require('../config/auth');
const async = require('async');

//club model
const Club = require('../models/Club');
//Membership model
const Membership = require('../models/Membership');
//Posting model
const Posting = require('../models/Posting');

/* GET home page. */
router.get('/', async(req, res) => {
  res.render('index', { req: req });
});

//로그인 후 메인 : 전체클럽 중 가입하지 않은 클럽만 보여줘
router.get('/main', ensureAuthenticated, async(req, res) => {
  let clubArr = [];
  const now = new Date();
  const start = new Date(now.getFullYear(),now.getMonth(),now.getDate(),1,0,0);
  const end = new Date(now.getFullYear(),now.getMonth(),now.getDate()+1,0,59,59);

  try{
  await Membership.find({email:req.user.email },(er, memberships)=>{
  }).then(memberships=>{
    for(i in memberships){
      clubArr.push(memberships[i].clubId);
    }

    //async.퍼렐 : 1.클럽찾기 + 2.인기글찾기(오늘작성되었고,클릭수가 가장 높은 3개)
   async.parallel({
    clubsFind: function (callback) { Club.find({_id:{$nin:clubArr}}).exec((err, data) => callback(null, data)) },
    postingsFind: function (callback) {Posting.find({ writtendate: {$gte: start, $lt: end}}).sort({'click':'desc'}).limit(3).populate('clubId').exec((err,data)=> callback(null,data))},
    
  }, function (err, result) {
    const { clubsFind, postingsFind } = result;
    res.render('main', { 
      req: req, user:req.user, clubList:clubsFind, postings : postingsFind 
    });
  });
  });
   
}catch(err){
  console.log(err);
}
});


module.exports = router;
