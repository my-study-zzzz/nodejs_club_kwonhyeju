var express = require('express');
var router = express.Router();
// const clubLogic = require('../public/javascripts/logic/clubLogic');
const { ensureAuthenticated } = require('../config/auth');

//board model
const Board = require('../models/Board');

//board home 
router.get('/', ensureAuthenticated, (req, res) => {//로그인 확인 후 연결
    res.render('./menu/board', {
      name: req.user.name,
      req: req,
      clubList : "",
      boardActive : 'active',
      user:req.user
    });
  }
);

//club register
router.post('/register', (req, res) => {
   const {name, intro} = req.body;
   let errors = [];
   clubLogic.registerClub(req.body);
   res.redirect('/club');
  }
);

//보드네임으로 보드찾기. 해당클럽의 보드만!
router.get('/findByName', async (req,res)=>{
  try {
    const boards = await Board.find({clubId:req.query.clubId ,name:{$regex:req.query.name}});
    res.json(boards);
  } catch (err) {
    res.json({ message: err });
  }
});

//클럽아이디로 보드찾기
router.get('/findByClubId/:clubId',(req,res)=>{
  Board.find({clubId:req.params.clubId})
  .then(boards=>{
    res.json(boards);
  })
  .catch(err=>{
    res.json({ message: err });
  });
});

module.exports = router;
