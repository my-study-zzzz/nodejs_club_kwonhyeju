var express = require('express');
var router = express.Router();
const clubLogic = require('../public/javascripts/logic/clubLogic');
const membershipController = require('../public/javascripts/controller/membershipController');
const { ensureAuthenticated } = require('../config/auth');

//club model
const Club = require('../models/Club');
//Membership model
const Membership = require('../models/Membership');
//user model
const User = require('../models/User');
//board model
const Board = require('../models/Board');



//운영클럽 - 보드관리 페이지로 이동
router.get('/:id', ensureAuthenticated, (req, res) => {
  const id = req.params.id;
  let clubName = '';
  Club.findById({ _id: id }, { name: 1, _id: 0 })
    .then(result => clubName = result.name)
    .then(result => {
      Board.find({ clubId: id })
        .then(boards => {
          res.render('./club/boardManage',
            { id: id, req: req, user: req.user, clubActive: 'active', boards: boards, name: clubName });
        });
    });

});

//보드등록 post
router.post('/register', (req, res) => {
  const { name, clubId } = req.body;

  const newBoard = new Board({ clubId: clubId, name: name });
  newBoard.save()
    .then(board => {
     res.json(board);
    });

});

//보드 한개 찾기 - 이름 + 클럽아이디
router.post('/findByNameAndClubId',async (req,res)=>{
  const {name, clubId} = req.body;
  try {
    const findBoard = await  Board.findOne({name:name, clubId: clubId});//save()가 promise를 반환한다.
    res.json(findBoard);
} catch (err) {
    res.json({ message: err });
}
});

//보드 한개 찾기 - 이름 +새이름 +클럽아이디
router.post('/findByNewNameAndClubId',async (req,res)=>{
  const {boardId, newName, clubId} = req.body;
  try {
    const findBoard = await  Board.findOne({_id: {$ne:boardId}, name: newName, clubId: clubId});//save()가 promise를 반환한다.
    res.json(findBoard);
} catch (err) {
    res.json({ message: err });
}
});

//보드 한개 삭제
router.get('/remove/:boardId',(req,res)=>{
  Board.deleteOne({_id:req.params.boardId})
  .then(board=>res.json({status: "SUCCESS"}));
});

//보드 수정
router.post('/update', (req, res) => {
  const { boardId, newName } = req.body;

  Board.updateOne({_id:boardId},{$set:{name:newName}})
    .then(board => {
     res.json(board);
    });

});

module.exports = router;
