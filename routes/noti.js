var express = require('express');
var router = express.Router();
const { ensureAuthenticated } = require('../config/auth');
const async = require('async');

//club model
const Club = require('../models/Club');
//Membership model
const Membership = require('../models/Membership');
//user model
const User = require('../models/User');
//Board model
const Board = require('../models/Board');
//Posting model
const Posting = require('../models/Posting');
//Comment model
const Comment = require('../models/Comment');
//Comment model
const Noti = require('../models/Noti');

router.get('/main',ensureAuthenticated,(req,res)=>{
  Noti.find({memId:req.user._id}).sort({'writtendate':'desc'})
  .then(notis=>{
    res.render('./menu/noti',
    { req: req, user: req.user, notis:notis });
  });
});




//알림 작성
router.post('/register', (req, res) => {
  const {title, content,memId} = req.body;
  const newNoti = new Noti({
    title,
    content,
    memId
  });

  newNoti.save().then(result=>{ res.json(result);});;

});







module.exports = router;
