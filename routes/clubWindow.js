var express = require('express');
var router = express.Router();
const clubLogic = require('../public/javascripts/logic/clubLogic');
const membershipController = require('../public/javascripts/controller/membershipController');
const { ensureAuthenticated } = require('../config/auth');
const { isMember } = require('../config/isMember');
const async = require('async');

//club model
const Club = require('../models/Club');
//Membership model
const Membership = require('../models/Membership');
//user model
const User = require('../models/User');
//Board model
const Board = require('../models/Board');
//Posting model
const Posting = require('../models/Posting');
//Comment model
const Comment = require('../models/Comment');


//클럽아이디와 함께 메인으로.
router.get('/main/:clubId', ensureAuthenticated, isMember, (req, res) => {
  const id = req.params.clubId;

  Club.findById(id)
    .then(club => {
      Board.find({ clubId: id })
        .then(boards => {
          res.render('./clubWindow/clubWindowMain',
            { req: req, user: req.user, window: true, main: true, club: club, boards: boards });
        });
    });
});

//게시판 클릭 - 포스팅리스트
router.get('/posting', ensureAuthenticated, isMember,(req, res) => {

  const clubId = req.query.clubId;
  const boardId = req.query.boardId;
  const nowPage = req.query.nowPage == null ? 1 : req.query.nowPage;
  const page = nowPage-1;
  
  async.parallel({
      clubFind: function (callback) { Club.findOne({ _id: clubId }).exec((err, data) => callback(null, data)) },
      boardsFind: function (callback) { Board.find({ clubId: clubId }).exec((err, data) => callback(null, data)) },
      boardOneFind: function (callback) { Board.findById(boardId).exec((err, data) => callback(null, data)) },
      postingsFind: function (callback) {Posting.find({ boardId: boardId }).sort({'writtendate':'desc'}).skip(10*page).limit(10).populate('memId').exec((err,data)=> callback(null,data))},
      postingsCount: function (callback) {Posting.countDocuments({ boardId: boardId }).exec((err, data) => callback(null, data))}
      
    }, function (err, result) {
      const { clubFind, boardsFind, boardOneFind, postingsFind,postingsCount } = result;
      res.render('./clubWindow/postingList', {
        req: req, user: req.user, window: true,
        club: clubFind, boardId: boardId, boardOne: boardOneFind, boards: boardsFind, postings: postingsFind,postingsCount:postingsCount, 
        nowPage : nowPage
      });
    });

});

//내가 쓴 글 보기 클릭 - 포스팅리스트
router.get('/myposting/:clubId', ensureAuthenticated, isMember, (req, res) => {

  const clubId = req.params.clubId;
  const nowPage = req.query.nowPage == null ? 1 : req.query.nowPage;
  const page = nowPage-1;
  
    async.parallel({
      clubFind: function (callback) { Club.findOne({ _id: clubId }).exec((err, data) => callback(null, data)) },
      boardsFind: function (callback) { Board.find({ clubId: clubId }).exec((err, data) => callback(null, data)) },
      postingsFind: function (callback) {Posting.find({ memId: req.user._id , clubId: clubId}).sort({'writtendate':'desc'}).skip(10*page).limit(10).populate('memId').exec((err,data)=> callback(null,data))},
      postingsCount: function (callback) {Posting.countDocuments({ memId: req.user._id, clubId: clubId }).exec((err, data) => callback(null, data))}
      
    }, function (err, result) {
      const { clubFind, boardsFind, boardOneFind, postingsFind,postingsCount } = result;
      res.render('./clubWindow/postingList', {
        req: req, user: req.user, window: true,
        club: clubFind, boards: boardsFind, postings: postingsFind,postingsCount:postingsCount, 
        nowPage : nowPage
      });
    });

});

//게시글 상세페이지로 이동
router.get('/postingInfo',isMember,(req, res) => {

  const clubId = req.query.clubId;
  const boardId = req.query.boardId;
  const postingId = req.query.postingId;
  const nowPage = req.query.nowPage == null ? 1 : req.query.nowPage;

  async.parallel({
    clubFind: function (callback) { Club.findOne({ _id: clubId }).exec((err, data) => callback(null, data)) },
    boardsFind: function (callback) { Board.find({ clubId: clubId }).exec((err, data) => callback(null, data)) },
    boardOneFind: function (callback) { Board.findById(boardId).exec((err, data) => callback(null, data)) },
    postingFind: function (callback) {
      Posting.findById(postingId).populate('memId').exec((err, data) => {
        if (data.memId._id != req.user.id) {
          Posting.findOneAndUpdate({ _id: postingId }, {$inc : {'click' : 1}},{new:true}).populate('memId')
          .exec((err, posting) => callback(null, posting));
        }else{
          callback(null, data);
        }
      })
    },
    commentsFind: function (callback) { Comment.find({ postingId: postingId }).populate('memId').exec((err, data) => callback(null, data)) },
  }, function (err, result) {
    const { clubFind, boardsFind, boardOneFind, postingFind,commentsFind} = result;
    res.render('./clubWindow/postingInfo', {
      req: req, user: req.user, window: true,
      club: clubFind, boardId: boardId, boardOne: boardOneFind, boards: boardsFind, posting: postingFind,comments:commentsFind,
      nowPage : nowPage
    });
  });

});

//내 게시글 상세페이지 이동
router.get('/mypostingInfo',isMember, (req, res) => {

  const clubId = req.query.clubId;
  const postingId = req.query.postingId;
  const nowPage = req.query.nowPage == null ? 1 : req.query.nowPage;

  async.parallel({
    clubFind: function (callback) { Club.findOne({ _id: clubId }).exec((err, data) => callback(null, data)) },
    boardsFind: function (callback) { Board.find({ clubId: clubId }).exec((err, data) => callback(null, data)) },
    postingFind: function (callback) {
      Posting.findById(postingId).populate('memId').populate('boardId').exec((err, data) => {
          callback(null, data);
      })
    },
    commentsFind: function (callback) { Comment.find({ postingId: postingId }).populate('memId').exec((err, data) => callback(null, data)) },
  }, function (err, result) {
    const { clubFind, boardsFind, postingFind, commentsFind } = result;
    res.render('./clubWindow/mypostingInfo', {
      req: req, user: req.user, window: true,
      club: clubFind,boards: boardsFind, posting: postingFind,comments:commentsFind,
      nowPage : nowPage
    });
  });

});

//멤버가 아닐때,
router.get('/noMember/:id', (req, res) => {
  
  Club.findOne({ _id: req.params.id })
  .then(club=>{
    res.render('./clubWindow/noMember',{
      req: req, user: req.user, window: true,
      club:club
    });
  });
  
});

module.exports = router;
