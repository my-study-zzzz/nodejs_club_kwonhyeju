var express = require('express');
var router = express.Router();
const { ensureAuthenticated } = require('../config/auth');

//club model
const Club = require('../models/Club');
//Membership model
const Membership = require('../models/Membership');

//myclub home  : 내가 가입한 클럽만 role:member
router.get('/', ensureAuthenticated, async (req, res) => {
  try {
    let clubArr = [];//내가 가입한 클럽만(운영클럽 X)
    await Membership.find({ email: req.user.email, role: 'Member' }, (er, memberships) => {

    }).then(memberships => {
      for (ms of memberships) {
        clubArr.push(ms.clubId);
      }
      Club.find({ _id: { $in: clubArr } }, (er, clubs) => {
      })
        .then(clubs => {
          res.render('./menu/myclub', {
            req: req,
            clubList: clubs,
            myclubActive: 'active',
            user: req.user
          });
        });
    });

  } catch (err) {
    // res.json({ message: err });
    console.log(err);
  }

});

//멤버십 셀프로 삭제
router.get('/remove/:id', async (req, res) => {
  const clubId = req.params.id;
  //클럽에서 멤버십 -1 => 멤버십삭제
  Club.updateOne({ _id: clubId }, { $inc: { memberCount: -1 } })
    .then(result => {
      Membership.deleteOne({ clubId: clubId, memId: req.user._id })
        .then(result => res.json({ message: 'SUCCESS' }));
    });
});

module.exports = router;