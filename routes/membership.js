var express = require('express');
var router = express.Router();
const clubLogic = require('../public/javascripts/logic/clubLogic');
const { ensureAuthenticated } = require('../config/auth');

//club model
const Club = require('../models/Club');
//user model
const User = require('../models/User');
//Membership model
const Membership = require('../models/Membership');



//find onemembership
router.get('/findOne/:email', ensureAuthenticated ,async (req, res) => {
  try {
    const membership = await Membership.findOne({email:req.params.email});
    res.json(club);
} catch (err) {
    res.json({ message: err });
}
});

//클럽아이디로 멤버 멤버십찾기(운영자 빼고)
router.get('/findMemberByClubId/:clubId', ensureAuthenticated ,async (req, res) => {
  try {
    const memberships = await Membership.find({clubId:req.params.clubId , role:'Member'});
    res.json(memberships);
} catch (err) {
    res.json({ message: err });
}
});



//membership register fetch
router.get('/registerFetch/:id',  (req, res)=>{
  try{
    const clubId = req.params.id;
    let memId = '';
    
    const newMembership = new Membership({
      clubId : clubId,
      memId : req.user._id,
      email : req.user.email,
      role: 'Member'
    });

    newMembership.save()
    .then(savedMemberhship=> {
      Club.updateOne({_id:clubId},{ $inc: { memberCount: 1 }})
      .then(result=>{
        res.json(savedMemberhship);
      });
    });
      
  }catch(err){
    res.json({message:err});
  }
  
});



//닉네임으로 멤버십 찾기
router.get('/findMembershipsByNick', async(req,res)=>{
  const nick = req.query.nick;
  const clubId = req.query.clubId;

  //해당클럽에 가입한 멤버십만 찾아야됨.
  //1.닉으로 멤버아이디 찾기
  try{
    let userArr = [];
    await User.find({nickname: {$regex : nick}})
    .then((users)=>{
      if(users == null) {
        res.json({message:'FAIL'});
        return; 
      }
      for(us of users){
        userArr.push(us._id);
      }
      Membership.find({clubId:clubId, memId : {$in : userArr}}).populate('memId')
      .then((memberships)=>{
        res.json(memberships);
      });
    })
  }catch(err){
    console.log(err);
    res.json({message: err});
  }

});

//이메일로 멤버십 찾기
router.get('/findMembershipsByEmail', async (req,res)=>{
  const {email, clubId} = req.query;
  try{
    await Membership.find({clubId:clubId, email :{$regex : email}}).populate('memId')
    .then((memberships)=>{
      res.json(memberships);
    });
  }catch(err){
    res.json({message: err});
  }
});


//운영클럽이 있는지
router.get('/isPresident',(req,res)=>{
  Membership.find({memId:req.user._id, role:'President'})
  .then(memberships=>res.json(memberships))
  .catch((err)=>res.json({message:err}));
});

module.exports = router;
