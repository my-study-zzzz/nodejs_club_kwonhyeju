var express = require('express');
var router = express.Router();
const clubLogic = require('../public/javascripts/logic/clubLogic');
const membershipController = require('../public/javascripts/controller/membershipController');
const { ensureAuthenticated } = require('../config/auth');
const async = require('async');

//club model
const Club = require('../models/Club');
//Membership model
const Membership = require('../models/Membership');
//user model
const User = require('../models/User');
//Board model
const Board = require('../models/Board');
//Posting model
const Posting = require('../models/Posting');
//Comment model
const Comment = require('../models/Comment');

//findAllPostingsInTheBoard
router.get('/findAllPostingsInTheBoard/:boardId', ensureAuthenticated, (req, res) => {
  Posting.find({ boardId: req.params.boardId })
    .then(result => res.json(result));
});

//포스팅 작성
router.post('/register', (req, res) => {
  const { title, content, boardId, clubId } = req.body;
  const newPosting = new Posting({
    title: title,
    content: content,
    boardId: boardId,
    clubId: clubId,
    memId: req.user.id,
  });
  console.log(newPosting);

  newPosting.save((err, posting) => {
    Board.updateOne({_id:boardId},{ $inc: { postingCount: 1 }})
    .then(result=>{ res.json(posting);});
  });

});

//포스팅 업데이트
router.post('/update', (req, res) => {
  const { title, content, postingId } = req.body;
  Posting.updateOne({ _id: postingId }, {
    $set: { title, content }
  })
    .then(result => res.json({ message: 'SUCCESS' }));
});

//포스팅 삭제
router.get('/remove/:id', (req, res) => {
  //포스팅삭제시 => 댓글삭제
  Posting.findByIdAndRemove(req.params.id, function(err, posting){
    Comment.deleteMany({
      "postingId": posting.id
    })
    .then(result=>{
      Board.updateOne({_id:posting.boardId},{ $inc: { postingCount: -1 }})
     .then(result=>{ res.json({ message: 'SUCCESS' });});
    })
    .catch(err=>{
      res.json({message:err});
    }) 
  });

});

//포스팅 한개 find
router.get('/find/:id', (req, res) => {
  Posting.findById(req.params.id, (err, data) => {
    res.json(data);
  });

});

//포스팅 10개 find
router.get('/findPostings', (req, res) => {
  const boardId = req.query.boardId;
  const page = (Number)(req.query.page) - 1;
  Posting.find({ boardId: boardId })
    .sort({ 'writtendate': 'desc' })
    .skip(10 * page)
    .limit(10)
    .populate('memId')
    .exec((err, data) =>
   { 
    res.json(data)});
});

//포스팅 find : title 
router.get('/findPostingsByTitle', (req, res) => {
  const boardId = req.query.boardId;
  const title = req.query.title;

  Posting.find({ boardId: boardId, title: { $regex: title } })
    .sort({ 'writtendate': 'desc' })
    // .skip(10*page)
    // .limit(10)
    .populate('memId')
    .exec((err, data) => res.json(data));
});

//포스팅 find : content 
router.get('/findPostingsByContent', (req, res) => {
  const boardId = req.query.boardId;
  const content = req.query.content;

  Posting.find({ boardId: boardId, content: { $regex: content } })
    .sort({ 'writtendate': 'desc' })
    // .skip(10*page)
    // .limit(10)
    .populate('memId')
    .exec((err, data) => res.json(data));
});

//포스팅 find : writer 
router.get('/findPostingsByWriter', (req, res) => {
  const boardId = req.query.boardId;
  const nick = req.query.writer;

  //1.닉의 id찾고 -> 포스팅찾기
  async.waterfall([
    function (next) {
      User.findOne({ nickname: nick }).exec((err, user) => {
        if(user==null) res.json({message:'FAIL'});
        else next(err, user);
      });
    },
    function (user, next) {
      Posting.find({ boardId: boardId, memId: user._id })
      .sort({ 'writtendate': 'desc' })
      .populate('memId')
      .exec((err, postings) => {
        next(err, postings);
      });
    }
  ], function (err, result) {
    if(err) res.json({message:'FAIL'});
    res.json(result);
  });
});

//내가 쓴 포스팅 10개 find
router.get('/findMyPostings', (req, res) => {
  const page = (Number)(req.query.page) - 1;
  Posting.find({ memId: req.user._id , clubId: req.query.clubId})
    .sort({ 'writtendate': 'desc' })
    .skip(10 * page)
    .limit(10)
    .populate('memId')
    .exec((err, data) => res.json(data));
});

//포스팅 테스트 쌓기
router.get('/test', (req, res) => {
  const newPosting = new Posting({
    title: "테스트",
    content: "content",
    boardId: "5e8f0b3e5d4a2d2894470589",
    clubId: "5e8c61e43eb3d23dcc36d0fe",
    memId: "5e8a8702bcf3c018fc344612",
  });

  newPosting.save((err, posting) => {
    res.json(posting);
  });

});


router.get('/addPostings/:boardId', (req, res) => {

  Posting.find({boardId:req.params.boardId}).limit(10)
  .then(postings=>{
    for(posting of postings){
        new Posting({
          title: posting.title,
          content:posting.content,
          boardId: posting.boardId,
          clubId: posting.clubId,
          memId: posting.memId,
        }).save((err, posting) => {
      });
    }
  })
  .then(res.json({message:'SUCC'}));
  

});

router.get('/removePostings/:boardId',(req,res)=>{

  Posting.find({boardId:req.params.boardId}).limit(780)
  .then(postings=>{
    Posting.deleteMany({_id : {$in: postings} })
    .then(res.json({message:'SUCC'}));
  });
  
})

module.exports = router;
