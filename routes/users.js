const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const { ensureAuthenticated } = require('../config/auth');

//user model
const User = require('../models/User');

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});

//Login page
router.get('/login', (req, res) => {
  User.findOne({email:'khj@naver.com'}, function(err,user){
  });
  // res.json(User.find({}));
  res.render('./users/login', { req: req , loginActive: 'active', email:req.email});
}
);



//Register Page
router.get('/register', (req, res) => res.render('./users/register', { req: req }));

//Register handle
router.post('/register', async(req, res) => {
  const { name, email, password, password2, nickname, phonenumber, birthday } = req.body;
  let errors = [];

  //check required fields
  // if (!name || !email || !password || !password2) {
  //   errors.push({ msg: 'Please fill in all fields' });
  // }


  //check nickname duplication
  await User.findOne({ nickname: nickname })
    .then(user => {
      if (user){
        errors.push({ msg: '해당 닉네임이 이미 존재합니다.' });
      }
    });

  //check passwords match
  if (password !== password2) {
    errors.push({ msg: '패스워드가 일치하지 않습니다.' });
  }

  //check pass length
  if (password.length < 4) {
    errors.push({ msg: '비밀번호는 최소 4자 이상 입력바랍니다.' });
  }

  if (errors.length > 0) {
    res.render('./users/register', {
      errors,
      name,
      email,
      password,
      password2,
      nickname,
      phonenumber,
      birthday
    });
  } else {
    // validation passed
    User.findOne({ email: email })
      .then(user => {
        if (user) {
          //user exists
          errors.push({ msg: '해당 이메일이 이미 존재합니다.' });
          res.render('./users/register', {
            errors,
            name,
            email,
            password,
            password2,
            nickname,
            phonenumber,
            birthday,
          });
        } else {
          const newUser = new User({
            name,
            email,
            password,
            nickname,
            phonenumber,
            birthday,
          });

          // Hash Password (비밀번호 암호화)
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;

              //set password to hashed
              newUser.password = hash;
              //save user
              newUser.save()
                .then(user => {
                  req.flash('success_msg', '회원가입을 축하드립니다. 가입하신 이메일로 로그인 부탁드립니다.');
                  res.redirect('/users/login');
                })
                .catch(err => console.log(err));

            })
          });
        }
      });
  }
});

//Login post
router.post('/login',(req,res,next)=>{
  passport.authenticate('local',{
      successRedirect:'/main',
      failureRedirect: '/users/login',
      failureFlash: true,
  })(req,res,next);
});

//Logout post
router.get('/logout',(req,res)=>{
  req.logout();
  req.flash('success_msg','로그아웃 완료.');
  res.redirect('/users/login');
});


module.exports = router;
