
// yyyymmdd 형태로 포멧팅하여 날짜 반환
module.exports = {
    yyyymmdd : function (date)
{
    let yyyy = date.getFullYear().toString();
    let mm = (date.getMonth() + 1).toString();
    let dd = date.getDate().toString();
    return yyyy + '-' +(mm[1] ? mm : '0'+mm[0]) +'-' + (dd[1] ? dd : '0'+dd[0]);
}
}


