//Membership model
const Membership = require('../../../models/Membership');

module.exports = {
    findMembershipByClubId :  function(id){
       return Promise.resolve(Membership.find({clubId:id},{'email':1, '_id':0}));
    },
}