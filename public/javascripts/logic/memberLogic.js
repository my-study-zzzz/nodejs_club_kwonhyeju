

///////////////////////////////////멤버 함수

//이메일 validation 체크
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{3,}))$/;
    return re.test(email);
}

function validate(email) {
    if (validateEmail(email)) {//valid
        document.getElementById('emailInvalid').style.display = 'none';
        return true;

    } else {//invalid
        document.getElementById('emailInvalid').style.display = 'block';
        return false;
    }
}

function registerMember(obj) {
    if(!validateFormLength(obj )){
        return;
    }

    let email = obj.email.value;
    console.log('email:', email);
    if (email.length == 0 | !validateEmail(email)) {
        alert('올바른 이메일형식이 아닙니다.');
        return;
    }

    if (memberMap.get(obj.email.value) != null) {
        alert('이미 존재하는 이메일입니다.');
        return;
    }

    var member = new Member(email, obj.name.value, obj.phoneNumber.value, obj.nickName.value, obj.birthDay.value);

    createMember(member);

    // alert("멤버 입력성공!");

    setMemberList('all');
    setClearForm("memberForm");
}

function setMemberList(flag) {
    let html = '';
    memberMap.forEach((member) => {
        html += memberTableHTML(member);
    });

    document.getElementById("memberTable").innerHTML = html;
    memberFindEmailForm.word.value = '';
}

function setMemberSelectList(form) {
    let word = form.word.value;
    let selectedOption = form.postingSelector.value;
    let html = '';

    for (member of memberMap.values()) {
        if (switchConditionOfMember(selectedOption, word) == 'true') {
            html += memberTableHTML(member);
        }
    }

    document.getElementById("memberTable").innerHTML = html;
}

function switchConditionOfMember(selectedOption, word) {///셀렉터의 조건을 분기해준다
    switch (selectedOption) {
        case 'email':
            return `${member.email.includes(word)}`;
        case 'name':
            return `${member.name.includes(word)}`;
        case 'phoneNumber'://해당검색어를 포함하는지
            return `${member.phoneNumber.includes(word)}`;
        case 'nickName':
            return `${member.nickName.includes(word)}`;

    }
}

function memberTableHTML(member) {
    return `
    <tr>
      <td>${member.email}</td>
      <td>${member.name}</td>
      <td>${member.phoneNumber}</td>
      <td>${member.nickName}</td>
      <td>${member.birthDay}</td>
      <td><button class="btn btn-primary" onclick="modifyMemberOfList(this.value);" value="${member.email}">수정</button></td>
      <td><button class="btn btn-danger" onclick="removeMember(this.value);" value="${member.email}">삭제</button></td>
      <td><button class="btn btn-success" onclick="location.href='membership.html?email=${member.email}'" >멤버십</button></td>
    </tr>`;

}

function modifyMemberOfList(id) {//리스트에서 수정버튼 클릭!
    let targetMember = memberMap.get(id);
    let memberForm = document.getElementById("memberForm");
    memberForm.email.value = targetMember.email;
    memberForm.name.value = targetMember.name;
    memberForm.phoneNumber.value = targetMember.phoneNumber;
    memberForm.nickName.value = targetMember.nickName;
    memberForm.birthDay.value = targetMember.birthDay;

    memberForm.email.disabled = true;//이메일은 수정못하게 막는다

    let button = memberForm.registerBtn;

    button.setAttribute('value', '수정');
    button.setAttribute('onclick', `modifyMemberOfForm(this.form,'${id}');`);

    memberForm.appendChild(button);

}

function modifyMemberOfForm(obj, id) {//실제수정+반영하기
    if(!validateFormLength(obj)){
        return;
    }
    let member = memberMap.get(id);
    member.name = obj.name.value;
    member.phoneNumber = obj.phoneNumber.value;
    member.nickName = obj.nickName.value;
    member.birthDay = obj.birthDay.value;

    updateMember(member);

    setMemberList('all');
    setClearForm("memberForm");
    memberForm.email.disabled = false;//이메일 다시 수정가능하게

    let button = memberForm.registerBtn;
    button.setAttribute('value', '등록');
    button.setAttribute('onclick', "registerMember(this.form,'register');");
}

function removeMember(email) {
    console.log(email);
    let result = confirm('정말로 삭제하시겠습니까?');
    if (result) {
        //멤버 삭제->클럽의 멤버십리스트 -해당멤버십 삭제
        for (membership of retrieveMember(email).membershipList) {
            let club = retrieveClub(membership.clubId);
            let membershipOfClub = getMembershipFromClub(club,email);
            console.log(membershipOfClub);
            let index = club.membershipList.indexOf(membershipOfClub);
            club.membershipList.splice(index, 1);
            updateClub(club);
        }
        ////////////////////////////////

        //멤버삭제->포스팅삭제
        for(posting of postingMap.values()){
            if(posting.writerEmail == email){
                deletePosting(posting.usid);
            }
        }
         
        //멤버삭제->admin이면 게시판 삭제
        for(board of boardMap.values()){
            if(board.adminEmail == email){
                deleteBoard(board.clubId);
            }
        }
            

        deleteMember(email);
        setMemberList('all');
    } 
}



