///////////////////////////////////////////////멤버십 함수
function setMembershipSelector(){//폼 셀렉터 설정하기
    let html = '<option value="">먼저, 클럽을 선택해주세요.</option>';
    for(club of retrieveAllClubs()){
        html+=`
        <option value="${club.name}">${club.name}</option>
        `;
    }

    document.getElementById('selector').innerHTML = html;
}

function findMembershipByClubName(name) {
    // let club = retrieveClubByName(name);
    if (name == '') {
        alert('클럽을 선택해주세요.');
        return;
    } else {//해당클럽찾음
        // document.getElementById('noticeMembership').innerHTML = '멤버십에 등록하실 이메일과 역할을 선택해주세요.';
        document.getElementById('noticeMembership').style.display = 'block';
        membershipForm.email.disabled = false;
        membershipForm.role[0].disabled = false;
        membershipForm.role[1].disabled = false;
        membershipForm.role[1].checked = true;
    }
}

function registerMembership(obj) {
    let clubName = obj.selector.value;
    if(clubName==''){
        alert('클럽을 선택해주세요.');
        return;
    }

    if(!validateFormLength(obj)){
        return;
    }
    let email = obj.email.value;
    let role = obj.role.value;
    let clubId = retrieveClubByName(clubName).usid;

    let membership = new Membership(clubId, clubName, email, role);

    //해당이메일의 멤버가 실제 존재하는지 체크
    if (retrieveMember(email) == null) {//해당이메일의 멤버가 없다면
        alert(email + '에 해당하는 멤버가 없습니다.');
        return;
    }

    let targetClub = retrieveClub(clubId);
    let targetMember = memberMap.get(email);

    ///클럽: 이미 가입한 멤버인지 체크
    if (getMembershipFromClub(targetClub, email) != null) {
        alert('해당클럽에 이미 가입하셨습니다.');
        return;
    }

    ///멤버: 이미 가입한 멤버인지 체크
    if (getMembershipFromMember(targetMember, clubId) != null) {
        alert('해당클럽에 이미 가입하셨습니다.');
        return;
    }

    //클럽에 멤버십추가하기
    targetClub.membershipList.push(membership);
    updateClub(targetClub);


    //멤버에 멤버십추가하기
    
    targetMember.membershipList.push(membership);
    updateMember(targetMember);

    setMembershipList('all');
    //작업완료후 폼처리
    formClear();
}

function formClear() {//작업완료후 폼처리
    setClearForm('membershipForm');
    // document.getElementById('noticeMembership').innerHTML = '';
    document.getElementById('noticeMembership').style.display = 'none';
    membershipForm.selector.disabled = false;
    membershipForm.email.disabled = true;
    membershipForm.role[0].disabled = true;
    membershipForm.role[1].disabled = true;
    membershipForm.name.disabled = false;
}

function setMembershipList() {

    let html = '';

    for (club of clubMap.values()) {
        html += membshipTableHTML(club);
    }

    document.getElementById("membershipTable").innerHTML = html;
    membershipFindForm.word.value = '';
}

function setMembershipSelectList(form,clubId) {
    let word = form.word.value;
    let selectedOption = form.membershipSelector.value;
    
    fetch(switchConditionOfMembership(selectedOption,word,clubId))
    .then(res => { return res.json() })
    .then(memberships => {
        console.log('멤버십',memberships);
        document.getElementById("membershipTable").innerHTML = membershipTableHTML(memberships);
    })
    .catch(e=>{
      console.log('에러:',e);
      document.getElementById("membershipTable").innerHTML = `
      <td colspan="4">
      <div class="alert alert-dismissible alert-danger">
        <strong><i class="far fa-frown-open mr-2"></i> 검색어에 해당되는 멤버가 없습니다. </strong> 
      </div>
      </td>
      `;
    });

}

function switchConditionOfMembership(selectedOption, word, clubId) {
    switch (selectedOption) {
        case 'nick':
            return `/membership/findMembershipsByNick?clubId=${clubId}&nick=${word}`;
        case 'email':
            return `/membership/findMembershipsByEmail?clubId=${clubId}&email=${word}`;
    }
}

function membershipTableHTML(memberships) {//멤버십리스트를 순회하여 html반환해라
    let html = '';
    for (member of memberships) {
        html += 
        `
        <tr>
            <td>${member.email}</td>
            <td>${member.memId.nickname}</td>
            <td>${new Date(member.joindate).yyyymmdd()}</td>
            <td>${member.role === 'President' ? '운영자' : '멤버'}</td>
            <td>
                <button  class="btn btn-danger" onclick="removeMembership('${ member._id }');">강제탈퇴</button>
            </td>
        </tr>
        `;
    }
    return html;
}

function removeMembership(membershipId) {

    if (confirm('정말로 탈퇴시키겠습니까?')) {

        $.get(`/manageMember/remove/${membershipId}`,()=>{
            location.reload();
        });
    }
}

function deleteMembership(clubId, email) {

    let targetClub = retrieveClub(clubId);
    let targetMember = retrieveMember(email);
    let targetMembership = getMembershipFromClub(targetClub, email);

    let index = targetClub.membershipList.indexOf(targetMembership);//멤버십리스트 배열의 몇번째 요소인가?
    targetClub.membershipList.splice(index, 1);//해당인덱스의 요소를 1개 삭제해라
    updateClub(targetClub);

    index = targetMember.membershipList.indexOf(targetMembership);
    targetMember.membershipList.splice(index, 1);
    updateMember(targetMember);

    //멤버십삭제->포스팅삭제
    for(posting of postingMap.values()){
        if(posting.clubId == clubId && posting.writerEmail == email){
            deletePosting(posting.usid);
        }
    }

    //멤버십삭제->(admin이면) 보드삭제
    for(board of boardMap.values()){
        if(board.clubId == clubId && board.adminEmail == email){
            deleteBoard(board.clubId);
        }
    }
}


function modifyMembership(obj) {
    let clubId = obj.value.split('-')[0];
    let email = obj.value.split('-')[1];

    let targetClub = retrieveClub(clubId);
    let targetMembership = getMembershipFromClub(targetClub, email);

    membershipForm.selector.value = targetClub.name;
    membershipForm.selector.disabled = true;
    membershipForm.email.value = email;
    membershipForm.email.disabled = true;

    let roleForm = membershipForm.role;
    roleForm[0].disabled = false;
    roleForm[1].disabled = false;
    if (targetMembership.role == 'President') {
        roleForm[0].checked = true;
    } else {
        roleForm[1].checked = true;
    }


    let button = membershipForm.registerBtn;
    button.setAttribute('value', '수정');
    button.setAttribute('onclick', `updateMembership(this.form,'${clubId}','${email}');`);
}

function updateMembership(obj, clubId, email) {
    if(!validateFormLength(obj)){
        return;
    }
    let newRole = obj.role.value;

    let targetMember = retrieveMember(email);
    let targetClub = retrieveClub(clubId);
    let targetMembership = getMembershipFromClub(targetClub, email);
    targetMembership.role = newRole;

    //클럽
    updateClub(targetClub);

    //멤버
    //클럽내의 멤버십객체 != 멤버내의 멤버십객체
    targetMember.membershipList.forEach(membership => {
        if (membership.clubId == clubId) {
            membership.role = newRole;
        }
    });

    //멤버
    updateMember(targetMember);

    setMembershipList('all');
    //작업완료 후 처리
    formClear();

    let button = membershipForm.registerBtn;
    button.setAttribute('value', '등록');
    button.setAttribute('onclick', `registerMembership(this.form,'register');`);
}

function getMembershipFromClub(targetClub, email) {
    let targetMembership = null;
    targetClub.membershipList.forEach(membership => {
        if (membership.email == email) {
            targetMembership = membership;
        }
    });
    return targetMembership;
}

function getMembershipFromMember(targetMember, clubId) {
    let targetMembership = null;
    targetMember.membershipList.forEach(membership => {
        if (membership.clubId == clubId) {
            targetMembership = membership;
        }
    });
    return targetMembership;
}

Date.prototype.yyyymmdd = function()
{
    let yyyy = this.getFullYear().toString();
    let mm = (this.getMonth() + 1).toString();
    let dd = this.getDate().toString();
 
    return yyyy + '-' +(mm[1] ? mm : '0'+mm[0]) +'-' + (dd[1] ? dd : '0'+dd[0]);
}