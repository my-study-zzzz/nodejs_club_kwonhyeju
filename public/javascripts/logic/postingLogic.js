//////////////////////////////포스팅 함수


function setPostingList(boardId){
    
    $.get('/posting/findAllPostingsInTheBoard/'+boardId)
    .then(postings=>{

        console.log(postings);
        let html = makePostingMainHTML(postings);

        document.getElementById('contentDiv').innerHTML = html;
    });
    
}

function makePostingMainHTML(postings){

    let html = '';

    

    html += `
            <h3>게시판이름</h3>
            <div class="mb-4"></div>
            <table class="table table-hover">
                <tr>
                    <thead>
                        <th style="width:450px;text-align: center;">글제목</th>
                        <th>작성자</th>
                        <th>작성일</th>
                        <th>조회</th>
                       
                    </thead>
                    </tr>
                    <tbody id="postingTableBody">`;
                    for(posting of postings){
                        html+=
                        `<tr>
                        <td> posting.title </td>
                        <td> posting.nickname </td>
                        <td> posting.writtendate </td>
                        <td> posting.click </td>
                       
                    </tr>`;
                    }
                        
       html +=      `</tbody>   
            </table> 

            <hr> 

            <!--글쓰기 버튼-->
            <div class="text-right">
                 <button onclick="" class="btn btn-outline-success"" align="right" >글쓰기</button>
            </div>

            <hr>

            <!--페이지네이션-->
            <nav aria-label="Page navigation text-center">
                <ul class="pagination " style="justify-content: center;">
                  <li class="page-item disabled">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
                </ul>
              </nav>   

              <hr>

              <!--검색필터-->
              <div class="d-flex justify-content-center"  >
                <form class="form-inline" name="postingFindForm" style="margin:auto">
                    <select class="form-control" name="postingSelector">
                      <option value="title">글제목</option>
                      <option value="contents">글내용</option>
                      <option value="writer">작성자</option>
                    </select>
                    <input class="form-control mx-1" type="text" name="word" placeholder="" id="postingFinder">
                    <input class="btn btn btn-dark " type="button" value="Find" onclick="setPostingSelectList(this.form)" />
            
                  </form>
              </div>
    `;

    return html;

}


function removePosting(postingId, nowPage){
  const boardId = document.getElementById('boardId').innerText;
  const clubId = document.getElementById('clubId').innerText;
  if(confirm("정말로 삭제하시겠습니까?")){

    fetch(`/posting/remove/${postingId}`)
    .then(result=>{ 
      // location.href = document.referrer;
      // location.href = `/clubWindow/posting?boardId=${boardId}&clubId=${clubId}`;
      goToPostingList(Number(nowPage));
    });
  }

}


///////////////////////페이징을 위한 함수

function pagination(page, totalRecordCount) {//페이지네이션만 세팅해주는 함수
  const pageSize = 10;//한 페이지의 게시글 갯수
  const blockPage = 5;//페이지네이션 1-5
  // const totalRecordCount = Number('<%=postingsCount%>'); //전체포스팅 수
  const totalPage = Math.ceil(totalRecordCount / pageSize);//전체 페이지수
  const nowPage = typeof page == 'undefined'? 1 : page;

  let pagingHTML = makePagingHTML(totalPage, totalRecordCount, pageSize, blockPage, nowPage);

  document.getElementById('pagingUl').innerHTML = pagingHTML;
}


function makePagingHTML(totalPage, totalRecordCount, pageSize, blockPage, nowPage) {//이거 페이지 클릭할때마다 호출되면 안돼
  let pagingStr = '';

  let intTemp = Math.floor((nowPage - 1) / blockPage) * blockPage + 1;//intTemp:각 페이지의 첫번째 페이지 값 저장

  //처음 및 이전을 위한 로직 << <
  if (intTemp != 1) {
      pagingStr += "<li class=\"page-item\">\r\n" +
          "<a class=\"page-link\" href='javascript:paging(1);'>\r\n" +
          "<span aria-hidden=\"true\">&laquo;</span>\r\n" +
          "</a>\r\n" +
          "</li>\r\n" +
          "<li class=\"page-item\">\r\n" +
          "<a class=\"page-link\" href='javascript:paging(" + (intTemp - blockPage) + ");' aria-label=\"Previous\">\r\n" +
          "<span aria-hidden=\"true\">&lsaquo;</span>\r\n" +
          "</a>\r\n" +
          "</li>";
  }

  //페이지 표시 제어를 위한 변수
  let blockCount = 1;

  //페이지를 뿌려주는 로직
  //블락 페이지 수만큼 혹은 마지막 페이지가 될때까지 페이지를 표시한다1
  while (blockCount <= blockPage && intTemp <= totalPage) {  // 페이지 오버 를 체크
      //현재 페이지를 의미함
      if (intTemp == nowPage) {//active
          pagingStr += "<li class='active page-item'><a  class=\"page-link\" href='javascript:paging(" + intTemp + ");'>" + intTemp + "</a></li>";
      }
      else
          pagingStr += "<li class=\"page-item\" ><a class=\"page-link\"  href='javascript:paging(" + intTemp + ");'>" + intTemp + "</a></li>";

      intTemp = intTemp + 1;
      blockCount = blockCount + 1;
  }

  //다음 및 마지막을 위한 로직 > >>
  if (intTemp <= totalPage) {
      pagingStr += "<li>\r\n" +
          "<a class=\"page-link\" href='javascript:paging(" + intTemp + ");' aria-label=\"Next\">\r\n" +
          "<span aria-hidden=\"true\">&rsaquo;</span>\r\n" +
          "</a>\r\n" +
          "</li>\r\n" +
          "<li>\r\n" +
          "<a class=\"page-link\"  href='javascript:paging(" + totalPage + ");' >\r\n" +
          "<span aria-hidden=\"true\">&raquo;</span>\r\n" +
          "</a>\r\n" +
          "</li>";
  }

  return pagingStr;
}

function postingListMaking(postings,nowPage) {

  let html = '';
  if(postings.length > 0){
    for (posting of postings) {
      html +=
          `<tr>
              <td><a href="javascript:goToPostingInfoWithPage('${posting._id}',${nowPage});" class="postingTitle">  ${posting.title} </a></td>
              <td> ${posting.memId.nickname}</td>
              <td> ${new Date(posting.writtendate).yyyymmdd()} </td>
              <td> ${posting.click}</td>
             
          </tr>`;
    }
  }else{//만약 포스팅이 0개라면
    html = `
    <tr>
    <td colspan="5">
    <h5 align="center">검색어와 일치하는 게시글이 없습니다.</h5>
    </td>
    </tr>
    `;
  }
  

  document.getElementById('postingTableBody').innerHTML = html;
}




////////////////////////////검색필터
function setPostingSelectList(form) {//포스팅 find 클릭시
  const word = form.word.value;
  const boardId = getBoardId();
  
  const selectedOption = form.postingSelector.value;
  let html = '';

//&page=${nowPage}  필터검색시 페이징처리 X
  fetch(switchCondition(selectedOption,word,boardId))
  .then(res => { return res.json() })
  .then(postings => {
      postingListMaking(postings,1);
      document.getElementById("pagingUl").innerHTML = '';
      // pagination(nowPage, Number(`<%=postingsCount%>`));
  })
  .catch(e=>{
    console.log('에러:',e);
    document.getElementById("postingTableBody").innerHTML = `
    <tr>
    <td colspan="5">
    <h5 align="center">검색어와 일치하는 게시글이 없습니다.</h5>
    </td>
    </tr>
    `;
  });

}

function switchCondition(selectedOption, word, boardId) {///셀렉터의 조건을 분기해준다
  let condition = null;
  switch (selectedOption) {
      case 'title':
          condition = `/posting/findPostingsByTitle?boardId=${boardId}&title=${word}`;
          break;
      case 'contents'://해당검색어를 포함하는지
          condition = `/posting/findPostingsByContent?boardId=${boardId}&content=${word}`;
          break;
      case 'writer':
          condition = `/posting/findPostingsByWriter?boardId=${boardId}&writer=${word}`;
          break;
  }
  return condition;
}
















///////////////////dateUtil
Date.prototype.yyyymmdd = function()
{
    let yyyy = this.getFullYear().toString();
    let mm = (this.getMonth() + 1).toString();
    let dd = this.getDate().toString();
 
    return yyyy + '-' +(mm[1] ? mm : '0'+mm[0]) +'-' + (dd[1] ? dd : '0'+dd[0]);
}

Date.prototype.getTimeStamp = function()
{
  var s =
    leadingZeros(this.getFullYear(), 4) + '-' +
    leadingZeros(this.getMonth() + 1, 2) + '-' +
    leadingZeros(this.getDate(), 2) + ' ' +

    leadingZeros(this.getHours(), 2) + ':' +
    leadingZeros(this.getMinutes(), 2) + ':' +
    leadingZeros(this.getSeconds(), 2);

  return s;
}

function leadingZeros(n, digits) {
  var zero = '';
  n = n.toString();

  if (n.length < digits) {
    for (i = 0; i < digits - n.length; i++)
      zero += '0';
  }
  return zero + n;
}















