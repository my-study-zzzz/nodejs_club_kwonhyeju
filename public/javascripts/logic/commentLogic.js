function registerComment(postingId){
    const content = document.getElementById('commentContent').value;
    if(content.length == 0){
        alert('댓글 내용을 입력해주세요.');
        return;
    }
    let postdata = {
        'content': content,
        'postingId': postingId
    };

    fetch('/comment/register',{
        method: 'post',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(postdata)
    })
    .then(res=>{
        window.location.reload();
    })
    .catch(err=>{console.log(err)});
    
}

