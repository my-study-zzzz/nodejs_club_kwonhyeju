// const clubStore = require('../store/clubStore');
const common = require('./common');
//club model
const Club = require('../../../models/Club');

///////////////////////////////////클럽 함수
module.exports = {

    validateClub : function (name,intro) {

       if(name.length < 3){
            return {msg : '클럽이름은 3자 이상입력'};
       }else if(intro.length < 10){
            return {msg : '클럽인트로는 10자 이상입력'};
       }
    },


    registerClub : function (body) {//클럽등록
        // if (!common.validateFormLength(obj)) {
        //     return;
        // }

        const {name,intro} = body; 

        let club = null;
        try {
            club = new Club({name, intro});
            
            clubStore.createClub(club);

            this.setClubList('all');
            common.setClearForm("clubForm");

        } catch (e) {
            console.log('입력실패',e);
        }

    },

    setClubList : function (flag) {//클럽리스트 세팅
        let html = '';
        if (flag == 'all') {
            clubStore.retrieveAllClubs.forEach((item) => {
                html += clubTableHTML(item);
            });
        } else {
            html = clubTableHTML(flag);
        }
        document.getElementById("clubTable").innerHTML = html;
    },

    clubTableHTML : function (club) {
        return `
    <tr>
      <td>${club.name}</td>
      <td>${club.intro}</td>
      <td>${club.foundationDay}</td>
      <td><button class="btn btn-primary" onclick="setFormForModifyClub(this);" value="${club.id}">수정</button></td>
      <td><button class="btn btn-danger" onclick="removeClub(this);" value="${club.id}">삭제</button></td>
      <td><button class="btn btn-success" onclick="location.href='membership.html?clubId=${club.id}'" >멤버십</button></td>
    </tr>`;
    },

    findClubByName  : function (name) {///클럽이름으로 하나 찾아와
        let clubFound = retrieveClubByName(name);
        document.getElementById('clubFind').value = '';
        if (clubFound == null) {
            document.getElementById("clubTable").innerHTML = '';
            return;
        }
        setClubList(clubFound);
    },

    setFormForModifyClub : function (obj) {//클럽 수정
        let id = obj.value;
        let targetClub = retrieveClub(id);
        document.getElementById("clubName").value = targetClub.name;
        document.getElementById("clubIntro").value = targetClub.intro;

        let button = document.getElementById("registerClubBtn");

        button.setAttribute('value', '수정');
        button.setAttribute('onclick', `modifyClub(this.form,'${id}');`);

        document.getElementById("clubForm").appendChild(button);

    },

    modifyClub : function (form, id) {

        if (!validateFormLength(form)) {
            return;
        }


        let club = retrieveClub(id);

        let name = form.name.value;
        if (name.length < 3) {
            alert('클럽명은 최소 3자이상 입력하세요.');
            return;
        }
        if (form.intro.value.length < 10) {
            alert('클럽 인트로는 최소 10자이상 입력하세요.');
            return;
        }

        //클럽 네임 한번더 체크
        if (club.name != name && retrieveClubByName(name) != null) {
            alert('해당클럽명 이미 존재!');
            return;
        }

        club.name = name;
        club.intro = form.intro.value;

        //클럽의 이름이 바뀌면, 멤버십의 클럽명도 바뀌어야한다.
        //멤버십
        club.membershipList.forEach(item => {
            item.clubName = name;
        });

        updateClub(club);


        let button = document.getElementById("registerClubBtn");
        button.setAttribute('value', '등록');
        button.setAttribute('onclick', "registerClub(this.form,'register');");

        setClubList('all');
        setClearForm("clubForm");
    },

    removeClub : function (obj) {
        let result = confirm('정말로 삭제하시겠습니까?\r\n(해당클럽의 멤버십, 게시판도 함께 삭제됩니다.)');
        if (result) {
            let id = obj.value;

            //클럽삭제->보드삭제->포스팅삭제
            let postings = retrievePostingsByBoardId(id);
            for (posting of postings) {
                deletePosting(posting.usid);
            }
            //클럽삭제->보드삭제
            deleteBoard(id);


            //클럽삭제->멤버의 멤버십 삭제
            for (membership of retrieveClub(id).membershipList) {
                let member = retrieveMember(membership.email);

                let index = member.membershipList.indexOf(membership);
                member.membershipList.splice(index, 1);
                updateMember(member);
            }
            ///////////////////////////////////////////////////

            //클럽삭제
            deleteClub(id);

            setClubList('all');

        }
    },

    findAllClubs : function (){
        return clubStore.retrieveAllClubs();
    }
}




