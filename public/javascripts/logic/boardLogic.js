
///////////////////////////////////보드 함수


function registerBoard(form,id) {
    $.ajaxSetup({ async:false });

    let boardName = form.boardName.value;
    //보드네임 체크!
    if(boardName == ''){
        alert('게시판이름을 입력해주세요!');
        return;
    }

    var postdata = {
        'name' : boardName,
        'clubId' : id
    };

    let isDuple = false;
    $.post('/manageBoard/findByNameAndClubId', postdata, function(board) {
        if(board){
            alert('해당 이름의 게시판이 이미 존재합니다.');
            isDuple = true;
        }
    })
    .done(result=>{
        console.log("결과가 모냐구:",result);
        if(isDuple){
            return;
        }else{
            $.post('/manageBoard/register', postdata, function(board) {
                // setBoardList('all');
                // setClearBoardForm();
                location.reload();
            });
        }
    })

    
}



function setBoardList(name,clubId) {
    fetch(`/board/findByName?name=${name}&clubId=${clubId}`)
    .then(res=>res.json())
    .then(boards=>{
        if (boards.length == 0) {
            document.getElementById("boardTable").innerHTML = `
            <div class="alert alert-dismissible alert-danger">
              <strong><i class="far fa-frown-open mr-2"></i> 검색어에 해당되는 클럽이 없어요 </strong> 
          </div>
            `;
            return;
        }


        document.getElementById("boardTable").innerHTML = boardTableHTML(boards);
    });

}

function boardTableHTML(boards) {
    let html = '';
    for( board of boards){
        html += 
        `
        <hr>
        <div class="row">
            <div class="col-md-8"> 
                <h5 id="orgName"><i class="fas fa-clipboard-list mx-3"></i>${board.name}</h5>
            </div>
            <div class="col-md-1 mt-1">
              <i class="far fa-file-alt mx-1"></i>${board.postingCount}
            </div>
            <div class="col-md-3 ">
                <button class="btn btn-info" onclick="setFormForModifyBoard('${board._id}','${board.clubId}');" >수정</button>
                <button  class="btn btn-danger" onclick="removeBoard(this.value,${board.postingCount});" value="${board._id}">삭제</button>
            </div>
        </div>
        `;
    }
    return html;
}





function modifyBoard(form, boardId , clubId) {

    //보드네임 체크!
    let newName = form.boardName.value;
    //보드네임 체크!
    var postdata = {
        'boardId' : boardId,
        'newName':newName,
        'clubId' : clubId
    };

    let isDuple = false;
    $.post('/manageBoard/findByNewNameAndClubId', postdata, function(board) {
        if(board){
            alert('해당 이름의 게시판이 이미 존재합니다.');
            isDuple = true;
        }
    })
    .done(result=>{
        console.log("결과가 모냐구:",result);
        if(isDuple){
            return;
        }else{
            $.post('/manageBoard/update', postdata, function(board) {
                location.reload();
            });
        }
    })

}

function setClearBoardForm() {
    setClearForm('boardForm');
}



function removeBoard(id,postingCount) {
    if(Number(postingCount) > 0){
        alert('해당게시판에 게시글이 있어 삭제가 불가능합니다.');
        return;
    }

    let result = confirm('삭제하시겠습니까?');
    if(result){
       $.get(`/manageBoard/remove/${id}`,()=>{
         location.reload();
       });
    }
 }
  
  function setFormForModifyBoard(boardId, clubId) {
      
    document.getElementById("boardName").value = document.getElementById("orgName").innerText;
    document.getElementById("title").innerHTML = '게시판 수정';

    let button = boardForm.registerBtn;
    button.setAttribute('value', '수정');

    button.setAttribute('onclick', `modifyBoard(this.form,'${boardId}','${clubId}');`);

}

/////////////////////
$(document).ready(function (){
    

});