// import "@babel/polyfill";
// import Club from "./travelClub.js";
// localStorage.removeItem('clubMap');
// localStorage.removeItem('autoIdMap');
// localStorage.removeItem('postingMap');
// localStorage.removeItem('boardMap');
// localStorage.removeItem('memberMap');

// localStorage.setItem('clubMap', JSON.stringify([...new Map()]));
// localStorage.setItem('autoIdMap', JSON.stringify([...new Map()]));
// let clubMap = new Map(JSON.parse(localStorage.getItem('clubMap')));
// let autoIdMap = new Map(JSON.parse(localStorage.getItem('autoIdMap')));
// let memberMap = new Map(JSON.parse(localStorage.getItem('memberMap')));
// let boardMap = new Map(JSON.parse(localStorage.getItem('boardMap')));
// let postingMap = new Map(JSON.parse(localStorage.getItem('postingMap')));




//////////////////공통함수
module.exports = {
    validateFormLength : function (form){//폼 입력시 모든 항목 validate체크
        let inputs = form.getElementsByTagName('input');
        for(let i=0;i < inputs.length;i++){
            if(inputs[i].value.length==0){
                alert('모든 항목 입력바랍니다.');
                return false;
            }
        }
        return true;
    },
    
    getParam :function (sname) {//url에서 parameter추출
    
        let params = location.search.substr(location.search.indexOf("?") + 1);
    
        let sval = "";
    
        params = params.split("&");
    
        for (let i = 0; i < params.length; i++) {
    
            temp = params[i].split("=");
    
            if ([temp[0]] == sname) { sval = temp[1]; }
    
        }
    
        return sval;
    
    },
    

    
    setClearForm : function (id) {//폼 초기화
        document.getElementById(id).reset();
    },
    
    setLocalStorage : function (key, map) {///localStorage세팅해줘
        localStorage.setItem(key, JSON.stringify([...map]));
    }
    
    
    
 
    
    
}

    // yyyymmdd 형태로 포멧팅하여 날짜 반환
    Date.prototype.yyyymmdd = function()
    {
        let yyyy = this.getFullYear().toString();
        let mm = (this.getMonth() + 1).toString();
        let dd = this.getDate().toString();
     
        return yyyy + '-' +(mm[1] ? mm : '0'+mm[0]) +'-' + (dd[1] ? dd : '0'+dd[0]);
    }
    
    
    String.format = function(id){//id:1-->00001
        let str = "" + id
        let pad = "00000"
        let ans = pad.substring(0, pad.length - str.length) + str;
        return ans;
    }

    class Club {
    
        constructor(name, intro) {
            this.setName(name);
            this.setIntro(intro);
            this.membershipList = [];
            this.foundationDay = new Date().yyyymmdd();
        }
    
        setUsid(usid){
            this.usid = usid;
            console.log('usid!');
        }
    
        setName(name) {
            if (name.length < 3) {
                alert('클럽명은 최소 3자이상 입력하세요.');
                throw new Error();
            }
            else {
                this.name = name;
            }
        }
    
        setIntro(intro) {
            if (intro.length < 10) {
                alert('클럽 인트로는 최소 10자이상 입력하세요.');
                throw new Error();
            } else {
                this.intro = intro;
            }
        }
    }
    
    
    class Member {
        constructor(email, name, phoneNumber, nickName, birthDay) {
            this.email = email;
            this.name = name;
            this.phoneNumber = phoneNumber;
            this.nickName = nickName;
            this.birthDay = birthDay;
            this.membershipList = [];
        }
    }
    
    class Membership{
        constructor(clubId, clubName, email, role){
            this.clubId = clubId;
            this.clubName = clubName;
            this.email = email;
            this.role = role;
            this.joindate = new Date().yyyymmdd();
        }
    }
    
    class Board{
        constructor(clubId,boardName,adminEmail){
            this.clubId = clubId;
            this.sequence = 0;
            this.boardName = boardName;
            this.adminEmail = adminEmail;
            this.createDate = new Date().yyyymmdd();
        }
    
    }
    
    class Posting{
        constructor(title,writerEmail,contents,boardId){
            this.setUsid(boardId);
            this.title = title;
            this.writerEmail = writerEmail;
            this.contents = contents;
            this.boardId = boardId;
            this.writtenDate = new Date().yyyymmdd();
        }
    
        setUsid(boardId){
            let board = boardMap.get(boardId);
            this.usid = boardId+":"+String.format(++board.sequence);
            boardMap.set(boardId,board);
            setLocalStorage('boardMap',boardMap);
        }
    }