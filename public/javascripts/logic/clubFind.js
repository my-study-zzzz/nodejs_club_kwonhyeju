////////////////////운영클럽

function findClubByName(name){
    fetch('/club/findByName/'+name)
    .then(res=>res.json())
    .then(clubs=>{
      if (clubs.length == 0) {
          document.getElementById("clubTable").innerHTML = `
        <div class="alert alert-dismissible alert-danger">
            <strong><i class="far fa-frown-open mr-2"></i> 검색어에 해당되는 클럽이 없어요 </strong> 
        </div>
          `;
          return;
      }
      setClubList(clubs);
    });

  }

  function setClubList (clubs) {//클럽리스트 세팅
      let html = clubTableHTML(clubs);
      document.getElementById("clubTable").innerHTML = html;
  }

  function clubTableHTML (clubs) {
    let html = '';
    
      for(club of clubs){
        html += `
        <div class="row p-2" >
          <div class="col-md-9">
            <a href="javascript:openWindow('${club.id}')" style="color:black"><h4>${club.name}</h4></a>
            <p>${club.intro}</p>
          </div>
          <div class="col-md-1 mt-4">
            <!-- <i class="fas fa-user"></i>8멤수 -->
          </div>
          <div class="col-md-2 ">
            <button class="btn btn-success mt-3" onclick="location.href='/manageClub/${club._id}'">관리</button>
          </div>
       </div>
      <hr>
        `;
      }
      return html;

  }


  ////////////////////////내클럽
  function findByNameMyClub(name){
    fetch('/club/findByNameMyClub/'+name)
    .then(res=>res.json())
    .then(clubs=>{
      if (clubs.length == 0) {
          document.getElementById("clubTable").innerHTML = `
          <td colspan="4">
            <div class="alert alert-dismissible alert-danger">
                <strong><i class="far fa-frown-open mr-2"></i> 검색어에 해당되는 클럽이 없어요 </strong> 
            </div>
          </td>
          `;
          return;
      }
      document.getElementById("clubTable").innerHTML = myclubTableHTML(clubs);
    });

  }

  function myclubTableHTML (clubs) {
    let html = '';
    
      for(club of clubs){
        html += `
        <tr>
          <td><a href="javascript:openWindow('${club._id}')" style="color:black;font-weight: bold;">${club.name}</a></td>
          <td>${club.intro}</td>
          <td>${new Date(club.foundationDay).yyyymmdd()}</td>
          <td><button id="removeBtn" class="btn btn-danger" onclick="removeMembershipSelf('${club._id}');">탈퇴</button></td>
        </tr>
        `;
      }
      return html;

  }

  Date.prototype.yyyymmdd = function()
  {
      let yyyy = this.getFullYear().toString();
      let mm = (this.getMonth() + 1).toString();
      let dd = this.getDate().toString();
   
      return yyyy + '-' +(mm[1] ? mm : '0'+mm[0]) +'-' + (dd[1] ? dd : '0'+dd[0]);
  }