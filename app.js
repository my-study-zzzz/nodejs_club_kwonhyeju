const createError = require('http-errors');
const expressLayouts = require('express-ejs-layouts');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
require('dotenv/config');

const app = express();


//Passport config
require('./config/passport')(passport);

//Connect to Mongo
mongoose.connect(
    process.env.DB_CONNECTION,{useNewUrlParser:true, useUnifiedTopology: true})
    .then(()=>console.log('MongoDB connected...'))
    .catch(err=> console.log(err));

//EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

//BodyParser
// app.use(express.urlencoded({extended:false}));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: true}));

//Express session
app.use(session({
  secret: 'secret', 
  resave: true,
  saveUninitialized: true,
}));

//Passport middleware
app.use(passport.initialize());
app.use(passport.session());

//connect flash
app.use(flash());

//Global vars
app.use((req,res,next)=>{
  res.locals.errors = req.flash('errors');
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.name = req.flash('name');
  res.locals.intro = req.flash('intro');
  next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Router
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/club', require('./routes/club'));
app.use('/board', require('./routes/board'));
app.use('/membership', require('./routes/membership'));
app.use('/myclub', require('./routes/myclub'));
app.use('/mypage', require('./routes/mypage'));
app.use('/manageClub', require('./routes/manageClub'));
app.use('/manageBoard', require('./routes/manageBoard'));
app.use('/manageMember', require('./routes/manageMember'));
app.use('/clubWindow', require('./routes/clubWindow'));
app.use('/posting', require('./routes/posting'));
app.use('/comment', require('./routes/comment'));
app.use('/noti', require('./routes/noti'));

//얘네는 무조건 맨마지막에 와야해!!!!
// catch 404 and forward to error handler 
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
  // next();
});

module.exports = app;
